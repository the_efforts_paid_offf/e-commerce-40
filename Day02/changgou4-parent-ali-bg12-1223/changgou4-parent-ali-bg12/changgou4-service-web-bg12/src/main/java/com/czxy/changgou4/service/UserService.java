package com.czxy.changgou4.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.czxy.changgou4.pojo.User;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
public interface UserService extends IService<User> {
    /**
     * 通过用户名查询
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 通过手机号查询
     * @param mobile
     * @return
     */
    User findByMobile(String mobile);
}
