package com.czxy.changgou4.controller;

import com.czxy.changgou4.pojo.User;
import com.czxy.changgou4.service.UserService;
import com.czxy.changgou4.vo.BaseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/checkusername")
    public BaseResult checkusername(@RequestBody User user) {
        // 查询用户名
        User findUser = userService.findByUsername(user.getUsername());
        // 判断
        if(findUser != null) {
            // 不可用
            return BaseResult.error("用户名不可用");
        } else {
            // 可用
            return BaseResult.ok("用户名可用");
        }
    }

    @PostMapping("/checkmobile")
    public BaseResult checkmobile(@RequestBody User user) {
        // 查询手机号
        User findUser = userService.findByMobile(user.getMobile());
        // 判断
        if(findUser != null) {
            // 不可用
            return BaseResult.error("手机号已存在");
        } else {
            // 可用
            return BaseResult.ok("手机号可用");
        }
    }
}
