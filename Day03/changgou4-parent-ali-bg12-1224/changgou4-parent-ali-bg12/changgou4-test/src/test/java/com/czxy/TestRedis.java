package com.czxy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
public class TestRedis {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void testDemo() {
        stringRedisTemplate.opsForValue().set("username","下课");
    }
}
