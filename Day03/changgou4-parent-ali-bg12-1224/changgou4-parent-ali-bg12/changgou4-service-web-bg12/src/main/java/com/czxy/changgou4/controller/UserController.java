package com.czxy.changgou4.controller;

import com.czxy.changgou4.pojo.User;
import com.czxy.changgou4.service.UserService;
import com.czxy.changgou4.vo.BaseResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping("/checkusername")
    public BaseResult checkusername(@RequestBody User user) {
        // 查询用户名
        User findUser = userService.findByUsername(user.getUsername());
        // 判断
        if(findUser != null) {
            // 不可用
            return BaseResult.error("用户名不可用");
        } else {
            // 可用
            return BaseResult.ok("用户名可用");
        }
    }

    @PostMapping("/checkmobile")
    public BaseResult checkmobile(@RequestBody User user) {
        // 查询手机号
        User findUser = userService.findByMobile(user.getMobile());
        // 判断
        if(findUser != null) {
            // 不可用
            return BaseResult.error("手机号已存在");
        } else {
            // 可用
            return BaseResult.ok("手机号可用");
        }
    }

    @PostMapping("/register")
    public BaseResult register(@RequestBody User user) {

        //1 校验数据
        // 1.1 密码和确认密码
        if(StringUtils.isBlank(user.getPassword())){
            return BaseResult.error("密码不能空");
        }
        if(! user.getPassword().equals(user.getRepassword())) {
            return BaseResult.error("密码和确认密码不一致");
        }


        // 1.2 验证码
        // 1) 获得用户输入的验证码
        String code = user.getCode();
        // 2) 获得redis缓存的验证码
        String key = "sms_register" + user.getMobile();
        String redisCode = stringRedisTemplate.opsForValue().get(key);
        // 3) 比较
        if(redisCode == null) {
            return BaseResult.error("验证码失效");
        }
        if(!redisCode.equals(code)) {
            return BaseResult.error("验证码错误");
        }

        //2 注册
        boolean result = userService.register( user );

        //3 提示
        if(result) {
            // 成功
            return BaseResult.ok("注册成功");
        } else {
            // 失败
            return BaseResult.error("注册失败");
        }

    }

}
