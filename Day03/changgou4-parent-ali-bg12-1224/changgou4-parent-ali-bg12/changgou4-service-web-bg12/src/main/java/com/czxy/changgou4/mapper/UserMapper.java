package com.czxy.changgou4.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.czxy.changgou4.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 通过手机号查询用户
     * @param mobile
     * @return
     */
    @Select("select * from tb_user where mobile = #{mobile}")
    User findByMobile(@Param("mobile") String mobile);
}
