package com.czxy.changgou4.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.czxy.changgou4.mapper.UserMapper;
import com.czxy.changgou4.pojo.User;
import com.czxy.changgou4.service.UserService;
import com.czxy.changgou4.utils.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Override
    public User findByUsername(String username) {
        // 条件
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);

        // 查询
        List<User> list = this.baseMapper.selectList(queryWrapper);

        if(list != null && list.size() >= 1) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public User findByMobile(String mobile) {
        return baseMapper.findByMobile(mobile);
    }

    @Override
    public boolean register(User user) {

        // 1 处理数据
        // 1.1 对密码进行加密
        String newPassowrd = BCrypt.hashpw(user.getPassword());
        user.setPassword(newPassowrd);

        // 1.2 系统自动生成：时间
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());

        // 2 添加用户
        int insert = baseMapper.insert(user);
        return insert == 1;
    }
}
