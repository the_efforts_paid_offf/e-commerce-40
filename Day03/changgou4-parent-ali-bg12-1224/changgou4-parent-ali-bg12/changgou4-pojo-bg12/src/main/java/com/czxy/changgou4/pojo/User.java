package com.czxy.changgou4.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
@TableName("tb_user")
@Data
public class User {
    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    @TableField("updated_at")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;

    private String email;
    private String mobile;
    private String username;
    private String password;            //密码
    @TableField(exist = false)
    private String repassword;          //确认密码
    @TableField(exist = false)
    private String code;                //验证码
    private String face;
    private String expriece;

}

/*
CREATE TABLE `tb_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Email',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '手机号码',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '昵称',
  `password` char(60) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `face` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `expriece` int(10) unsigned DEFAULT '0' COMMENT '经验值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_mobile_unique` (`mobile`),
  UNIQUE KEY `users_name_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
)
 */