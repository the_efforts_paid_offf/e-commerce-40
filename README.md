# 电商4.0

#### 介绍
电商4.0
SpringCloud项目
电商项目分为：前台和后台
前台：提供用户使用
后台：提供商家进行管理
#### 软件架构

![image-20220314150203491](https://gitee.com/the_efforts_paid_offf/picture-blog/raw/master/img/20220314150351.png)



架构图：

![img](https://gitee.com/the_efforts_paid_offf/picture-blog/raw/master/img/20220314150400.jpg)

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明
B2C

B2C是Business-to-Customer的缩写，而其中文简称为“商对客”。“商对客”是电子商务的一种模式，也就是通常说的直接面向消费者销售产品和服务商业零售模式。这种形式的电子商务一般以网络零售业为主，主要借助于互联网开展在线销售活动。B2C即企业通过互联网为消费者提供一个新型的购物环境——网上商店，消费者通过网络在网上购物、网上支付等消费行为。
案例：唯品会、乐蜂网

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


